CC=clang
CFLAGS=--pedantic --std=c99 -lm

main: main.c
	$(CC) $(CFLAGS) -o pi main.c
