#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <float.h>

float pi(float n) {
	int n_circle = 0;
	int n_total = 0;

	srand(time(NULL));

	for(float i = 0; i < n; i++)
	{
		int x = rand() % 2;
		int y = rand() % 2;

		int distance = pow(x, 2) + pow(y, 2);

		if(distance <= 1) {
			n_circle += 1;
		}
		n_total += 1;
	}

	printf("n_circle = %d\nn_total = %d\n", n_circle, n_total);

	return 4 * (float) n_circle / (float) n_total;
}

int main() {
	float pi_ans = pi(pow(10, 7));

	printf("pi = %f\n", pi_ans);
	return 0;
}
